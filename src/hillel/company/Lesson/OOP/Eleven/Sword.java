package hillel.company.Lesson.OOP.Eleven;

public class Sword implements Weaponable {

    String name;
    int Damage;

    public String getName() {
        return name;
    }

    public int getDamage() {
        return Damage;
    }

    public Sword(String name, int damage) {
        this.name = name;
        Damage = damage;
    }

    @Override
    public String toString() {
        return "Bow{" +
                "name='" + name + '\'' +
                ", Damage=" + Damage +
                '}';
    }
}
