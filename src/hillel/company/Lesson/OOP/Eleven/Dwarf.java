package hillel.company.Lesson.OOP.Eleven;

public class Dwarf extends Warrior {

    private String name;

    private String gander;

    private int age;

    private int height;

    private int strength;

    private int PhysicalDefence;

    public Dwarf(String name, String gander, int age, int height, int strength, int PhysicalDefence) {
        this.name = name;
        this.gander = gander;
        this.age = age;
        this.height = height;
        this.strength = strength;
        this.PhysicalDefence = PhysicalDefence;
    }

    @Override
    public String toString() {
        return "Dwarf{" +
                "name='" + name + '\'' +
                ", gander='" + gander + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", strength=" + strength +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getGander() {
        return gander;
    }

    public int getAge() {
        return age;
    }

    public int getHeight() {
        return height;
    }

    public int getStrength() {
        return strength;
    }

    public int getPhysicalDefence() {
        return PhysicalDefence;
    }

    public void dwarfGreeting() {
        System.out.println("Hello, My name " + getName() + " 'and am Dwarfs'" + " 'My height' : " + getHeight() + "" +
                " and my Age :" + getAge() + " My gender :" + getGander() + " 'my Strength' :" + getStrength() + " 'physicalDefence' :" + getPhysicalDefence() + " we can use ->" + Axe.axe.getName());
    }

    @Override
    public void attack() {
        System.out.println("We gnomes Go to Attack! " + "Strike is -> " + Axe.axe.getName() + " 'Attack Damage' ->" + (Axe.axe.getDamage() * getStrength()));
    }

    @Override
    public void defence() {
        System.out.println("We Gnomes -> " + "we will protect everyone!" + "and We used is ->>" + Armor.armor.getName() + " her protection :" + (Armor.armor.getArmorDefence())+getPhysicalDefence());
    }
}
