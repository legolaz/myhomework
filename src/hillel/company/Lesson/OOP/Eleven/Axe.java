package hillel.company.Lesson.OOP.Eleven;

public class Axe implements Weaponable {

    String name;
    int Damage;


    public String getName() {
        return name;
    }

    public int getDamage() {
        return Damage;
    }

    public Axe(String name, int damage) {
        this.name = name;
        Damage = damage;
    }

    @Override
    public String toString() {
        return "Axe{" +
                "name='" + name + '\'' +
                ", Damage=" + Damage +
                '}';
    }
}
