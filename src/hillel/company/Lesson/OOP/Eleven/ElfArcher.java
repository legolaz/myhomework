package hillel.company.Lesson.OOP.Eleven;

public class ElfArcher extends Elf {
    public ElfArcher(String name, String gender, int height, int age, int strength, int physicalDefence) {
        super(name, gender, height, age, strength, physicalDefence);
    }

    public void greeting() {
        System.out.println("Hello, my name -> " + getName() + " 'and am Archer' " + " 'height' : " + getHeight() +
                "  'Age' :" + getAge() + " 'gender' :" + getGender() + "  'Strength' ->>" + getStrength() + "  'physicalDefence' ->>" + getPhysicalDefence() + "  We can use ->" + Bow.bow.getName());
    }


    @Override
    public void attack() {
        System.out.println("We Archers Go on the attack" + " Shoot is  -> " + Bow.bow.getName() + " 'Attack Damage'->" + (Bow.bow.getDamage() + getStrength()));
    }

    @Override
    public void defence() {
        System.out.println("We Archers -> " + "we will protect everyone!" + "and We used is ->>" + Armor.armorOne.getName() + " her protection :" + (Armor.armor.getArmorDefence() + getPhysicalDefence()));
    }
}