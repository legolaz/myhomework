package hillel.company.Lesson.OOP.Eleven;

public class ElfSwordMan extends Elf implements Weaponable {
    public ElfSwordMan(String name, String gender, int height, int age, int strength, int physicalDefence) {
        super(name, gender, height, age, strength, physicalDefence);
    }

    public void greeting() {
        System.out.println("Hi, my name -> " + getName() + "  'and am elfSwordMan'" + "  Height : " + getHeight() +
                " 'Age' : " + getAge() + "  My Gender :" + getGender() + " Strength ->>>" + getStrength() + " My physicalDefence ->>>" + getPhysicalDefence() + " We can use -> " + swordOne.getName());
    }

    @Override
    public void attack() {
        System.out.println("We elfSwordMan Go to Attack " + "Strike is " + sword.getName() + " 'Strike Damage' :" + swordOne.getDamage() * getStrength());
    }

    @Override
    public void defence() {
        System.out.println("We SwordMan -> " + "we will protect everyone!" + "and We used is ->>" + armorOne.getName() + " her protection :" + Armor.armor.getArmorDefence() * getPhysicalDefence());
    }
}
