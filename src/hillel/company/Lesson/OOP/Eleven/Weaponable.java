package hillel.company.Lesson.OOP.Eleven;

public interface Weaponable {

    Bow bow = new Bow("Draconian", 250);
    Axe axe= new Axe("Crusaders",200);
    Sword sword= new Sword("Black night",120);
    Sword swordOne= new Sword("Excalibur",140);

    Armor armor =new Armor("Mithril Mail",62);
    Armor armorOne =new Armor("Elfin Mail",64);

}
