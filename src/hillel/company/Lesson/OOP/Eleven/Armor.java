package hillel.company.Lesson.OOP.Eleven;

public class Armor implements  Weaponable{

    String name;
    int ArmorDefence;

    public Armor(String name, int ArmorDefence) {
        this.name = name;
        this.ArmorDefence = ArmorDefence;
    }

    public String getName() {
        return name;
    }


    public int getArmorDefence() {
        return ArmorDefence;
    }
}
