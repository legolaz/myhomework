package hillel.company.Lesson.OOP.Eleven;

public class armyComposition {

    public static void main(String[] args) {

        Elf elfOne = new Elf("Анариэль", "female", 191, 125, 13,21);
        Elf elfTwo = new Elf("Леголас", "male", 182, 120, 15,30);

        Elf elfArcherOne = new ElfArcher("Аманиэль", "female", 194, 150, 21,78);
        ElfArcher elfArcherTwo = new ElfArcher("Лаириэль", "female", 192, 145, 21,21);
        ElfArcher elfArcherThree = new ElfArcher("Трандуил", "male", 191, 400, 22,10);

        ElfSwordMan ElfSwordManOne = new ElfSwordMan("Эариэль", "female", 180, 351, 12,35);
        ElfSwordMan ElfSwordManTwo = new ElfSwordMan("Идриль", "female", 189, 353, 14,45);
        ElfSwordMan ElfSwordManThee = new ElfSwordMan("Арвелия", "female", 179, 355, 16,22);
        ElfSwordMan ElfSwordManFour = new ElfSwordMan("Амариель", "female", 183, 358, 15,64);
        ElfSwordMan ElfSwordManFave = new ElfSwordMan("Белег", "male", 190, 351, 10,80);


        new armyComposition().greetings(elfOne, elfTwo, elfArcherThree, elfArcherTwo, elfArcherOne,
                ElfSwordManOne, ElfSwordManTwo, ElfSwordManThee, ElfSwordManFour, ElfSwordManFave);

        System.out.println();

        Dwarf dwarfOne = new Dwarf("Торин", "male", 45, 129, 24,13);
        Dwarf dwarfTwo = new Dwarf("Балин", "male", 65, 133, 20,30);
        Dwarf dwarfThree = new Dwarf("Двалин", "male", 46, 135, 22,42);
        Dwarf dwarfFour = new Dwarf("Глоин", "male", 40, 1125, 21,32);

        new armyComposition().dwarfsGreetings(dwarfOne, dwarfTwo, dwarfThree, dwarfFour);
        System.out.println();

        armyComposition armyComposition = new armyComposition();
        armyComposition.toAttack(dwarfOne, dwarfTwo, dwarfThree, dwarfFour, elfOne, elfTwo, elfArcherOne, elfArcherTwo, elfArcherThree, ElfSwordManOne, ElfSwordManTwo, ElfSwordManThee, ElfSwordManFour, ElfSwordManFave);

        System.out.println();

        armyComposition.toDefence(dwarfOne, dwarfTwo, dwarfThree, dwarfFour, elfOne, elfTwo, elfArcherOne, elfArcherTwo, elfArcherThree, ElfSwordManOne, ElfSwordManTwo, ElfSwordManThee, ElfSwordManFour, ElfSwordManFave);

    }

    private void toAttack(Warrior... warriors) {
        for (Warrior warrior : warriors) {
            warrior.attack();
        }
    }

    private void toDefence(Warrior... warriors) {
        for (Warrior warrior : warriors) {
            warrior.defence();
        }
    }

    private void greetings(Elf... elves) {
        for (Elf elf : elves) {
            elf.greeting();
        }
    }

    private void dwarfsGreetings(Dwarf... dwarves) {
        for (Dwarf dwarf : dwarves) {
            dwarf.dwarfGreeting();
        }
    }
}