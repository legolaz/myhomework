package hillel.company.Lesson.OOP.Eleven;

public abstract class Warrior {

    public abstract void attack();

    public abstract void defence();

}
