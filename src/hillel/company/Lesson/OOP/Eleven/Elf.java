package hillel.company.Lesson.OOP.Eleven;

public class Elf extends Warrior {

    private String name;

    private int height;

    private int age;

    private String gender;

    private int strength;

    private int physicalDefence;

    public Elf(String name, String gender, int height, int age, int strength, int physicalDefence) {
        this.name = name;
        this.gender = gender;
        this.height = height;
        this.age = age;
        this.strength = strength;
        this.physicalDefence = physicalDefence;
    }

    public String getName() {
        return name;
    }

    public int getHeight() {
        return height;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public int getStrength() {
        return strength;
    }

    public int getPhysicalDefence() {
        return physicalDefence;
    }

    public void greeting() {
        System.out.println("Hello, My name " + getName() + " 'and am elfin'" + "  My height -> " + getHeight() +
                "'my Age' : " + getAge() + "  my gender :" + getGender() + "  My Strength -> " + getStrength() + "  physicalDefence ->>" + getPhysicalDefence() + "  We can use ->" + Sword.sword.getName());
    }

    @Override
    public void attack() {
        System.out.println("We Elf's Go to Attack! " + "Strike is :" + Sword.sword.getName() + "Strike Attack" + (Sword.sword.getDamage() + getStrength()));
    }

    @Override
    public void defence() {
        System.out.println("We Elf's -> " +
                "we will protect everyone! " + " We used is ->>" + Armor.armorOne.getName() + " her protection :" + (Armor.armor.getArmorDefence() + getPhysicalDefence()));
    }
}